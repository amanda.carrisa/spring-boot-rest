package coba.Controller;


import coba.Controller.Detail.UserDetail;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType; //ini untuk return type pas accept itu
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@RequestMapping("users") //localhost:8080/users
public class UserController {

    HashMap<String, UserRest> mapping;

    @GetMapping
    public String getUsers(@RequestParam(value = "page", defaultValue = "1") int page, @RequestParam(value = "limit", defaultValue = "50") int limit){
        return "get page "+page+" limit "+limit;
    }

    @GetMapping(path = "/{userId}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<UserRest> getUser(@PathVariable String userId){
//        UserRest user = new UserRest();
//        user.setFirstName("firstname");
//        user.setLastName("lastname");


        if(mapping.containsKey(userId)){
            return new ResponseEntity<UserRest>(mapping.get(userId), HttpStatus.OK);
        }
        else{
            return new ResponseEntity<UserRest>(mapping.get(userId), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<UserRest> createUser(@Valid @RequestBody UserDetail userDetail){

        UserRest user = new UserRest();
        user.setFirstName(userDetail.getNamaDepan());
        user.setLastName(userDetail.getNamaBelakang());

        mapping.put(user.getFirstName(), user);


        return new ResponseEntity<UserRest>(user, HttpStatus.OK);
    }

    @PutMapping
    public String updateUser(){
        return "apdet";
    }

    @DeleteMapping
    public String deleteUser(){
        return "del";
    }

}
