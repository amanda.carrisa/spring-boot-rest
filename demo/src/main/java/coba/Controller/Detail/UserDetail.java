package coba.Controller.Detail;

import javax.validation.constraints.NotNull;

public class UserDetail {
    @NotNull(message = "nama depan jan kosong")
    private String namaDepan;
//    @NotNull(message = "nama blkg jan kosong")
    private String namaBelakang;

    public String getNamaDepan() {
        return namaDepan;
    }

    public void setNamaDepan(String firstName) {
        this.namaDepan = firstName;
    }

    public String getNamaBelakang() {
        return namaBelakang;
    }

    public void setNamaBelakang(String lastName) {
        this.namaBelakang = lastName;
    }


}
